#Owner = rohitvarma.r
#git status ; git add . ; git commit -m "addedd route table" ; git push -u origin master


provider "aws" {
  region = var.region
}

####### VPC #######
resource "aws_vpc" "my-appvpc" {
  cidr_block = var.cidr_vpc
  tags = {
    Name = "${var.env_prefix}-vpc"
  }
}

####### SUBNET-1 #######
resource "aws_subnet" "my-appsubnet" {
  cidr_block        = var.cidr_subnet-1
  vpc_id            = aws_vpc.my-appvpc.id
  availability_zone = var.az_zone
  tags = {
    Name = "${var.env_prefix}-subnet-1"
  }
}

####### SUBNET-2 #######
resource "aws_subnet" "my-appsubnet-2" {
  cidr_block        = var.cidr_subnet-2
  vpc_id            = aws_vpc.my-appvpc.id
  availability_zone = var.az_zone
  tags = {
    Name = "${var.env_prefix}-subnet-2"
  }
}

####### INTERNETGATEWAY #######
resource "aws_internet_gateway" "my-gateway" {
  vpc_id = aws_vpc.my-appvpc.id
  tags = {
    Name = "${var.env_prefix}-igw"
  }
}


####### ROUTE TABLE #######
/*resource "aws_route_table" "my-route-table" {
  vpc_id = aws_vpc.my-appvpc.id
  route {
    cidr_block = var.public_cidr
    gateway_id = aws_internet_gateway.my-gateway.id
  }
  tags = {
    Name = "${var.env_prefix}-routetable"
  }
}*/

####### ROUTE TABLE ASSOCIATION #######
/*resource "aws_route_table_association" "rt_subnet_assoc" {
  route_table_id = aws_route_table.my-route-table.id
  subnet_id      = aws_subnet.my-appsubnet.id
}*/

####### DEFAULT/MAIN ROUTE TABLE ASSOCIATION #######
resource "aws_default_route_table" "main-rtb" {
  default_route_table_id = aws_vpc.my-appvpc.default_route_table_id
  route {
    cidr_block = var.public_cidr
    gateway_id = aws_internet_gateway.my-gateway.id
  }
  tags = {
    Name = "${var.env_prefix}-main-rtb"
  }
}

####### SECURITY GROUP #######
resource "aws_security_group" "my-dev-sec" {
  name = "my-sec-grp"
  vpc_id = aws_vpc.my-appvpc.id
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = var.my_ip
  }
  ingress {
    from_port = 8080
    protocol = "tcp"
    to_port = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 8080
    protocol = "tcp"
    to_port = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.env_prefix}-sec-grp"
  }
}

/*data "aws_ami" "latest_amaz_ami" {
  most_recent = true
  owners = ["amazon"]
  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}*/

####### INSTANCE #######
resource "aws_instance" "my-app-server" {
  ami = var.ami_id
  instance_type = "t2.micro"
  #vpc_id = aws_vpc.my-appvpc.id
  subnet_id = aws_subnet.my-appsubnet.id
  vpc_security_group_ids = [aws_security_group.my-dev-sec.id]
  availability_zone = var.az_zone
  associate_public_ip_address = true
  key_name = "2204"
  tags = {
    Name = "${var.env_prefix}-instance"
  }
}

######## OUTPUTS #########
output "my-instance" {
  value = aws_instance.my-app-server.id
}

output "my-gateway" {
  value = aws_internet_gateway.my-gateway.id
}

/*output "my-route-table" {
  value = aws_route_table.my-route-table
}*/

output "my-def_rt" {
  value = aws_default_route_table.main-rtb
}