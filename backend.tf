terraform {
  backend "s3" {
    bucket = "terrafromlearningpath"
    key    = "terrafrom/terrafromlearningpath"
    region = "us-east-1"
  }
}
