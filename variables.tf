variable "cidr_vpc" {
  default = ""
}

variable "cidr_subnet-1" {
  default = ""
}

variable "az_zone" {
  default = ""
}

variable "region" {
  default = ""
}

variable "cidr_subnet-2" {
  default = ""
}

variable "env_prefix" {
  default = ""
}

variable "public_cidr" {
  default = ""
}

variable "my_ip" {
  default = ""
}

variable "ami_id" {
  default = ""
}